//#include <QCoreApplication>
#include <stdio.h>
#include <fstream>
#include <iostream>
#include <iostream>
#include <string>
#include<fstream>
#include<algorithm>  //泛型指针头文件
#include<list>
#include<deque>
#include<map>
#include<vector>
#include<set>
#include<functional>//使用事先定义的function object
#include<iterator>
#include <fstream>
#include <cstdlib>
#include <stdlib.h>
#include<math.h>
#include <cstdlib>
#include <ctime>
#include <iomanip>
using std::cout;
using std::cerr;
using std::endl;
using std::ifstream;
using std::ofstream;

#include <string.h>
#include <stdlib.h>
#ifndef _MSC_VER
#include <getopt.h>
#include <unistd.h>
#else
#include "XGetopt.h"
#endif
/**
 * function: 寻找不合适的bin序号对应的pose文件以及序号。
 *main :
 * */

int parseArgs(int argc,char **argv, char dir[255], int& start, int& end){
    start   = 0;
    end     = -1; // -1 indicates no limitation

    int  c;
    // from unistd.h
    extern char *optarg;
    extern int optind;

    cout << endl;
    while ((c = getopt (argc, argv, "s:e:")) != -1)
        switch (c)
        {
        case 's':
            start = atoi(optarg);
            if (start < 0) { cerr << "Error: Cannot start at a negative scan number.\n"; exit(1); }
            break;
        case 'e':
            end = atoi(optarg);
            if (end < 0)     { cerr << "Error: Cannot end at a negative scan number.\n"; exit(1); }
            if (end < start) { cerr << "Error: <end> cannot be smaller than <start>.\n"; exit(1); }
            break;
        }

    if (optind != argc-1) {
        cerr << "\n*** Directory missing ***\n" << endl;
        cout << endl
             << "Usage: " << argv[0] << "  [-s NR] [-e NR] directory" << endl << endl;

        cout << "  -s NR   start at scan NR (i.e., neglects the first NR scans)" << endl
             << "          [ATTENTION: counting starts with 0]" << endl
             << "  -e NR   end after scan NR" << "" << endl
             << endl;
        cout << "Reads pose files from directory/scan???.pose and converts them to directory/scan???.frames to be used by show." << endl;
        abort();
    }
    strncpy(dir,argv[optind],255);

#ifndef _MSC_VER
    if (dir[strlen(dir)-1] != '/') strcat(dir,"/");
#else
    if (dir[strlen(dir)-1] != '\\') strcat(dir,"\\");
#endif
    return 0;
}

double double_abs(double x){
    return x<0?(-x):x;
}

int main(int argc, char *argv[])
{
    argc = 6;
    argv[0] = "./Fnid_closetnum";
    argv[1] = "-s";
    argv[2] = "0";
    argv[3] = "-e";
    argv[4] = "170";
    argv[5] = "/home/qy/Clont_git/Data/";
    int start = 0, end = -1;
    char dir[255];
    parseArgs(argc, argv, dir, start, end);

    ifstream pose_out, uapose_out;
    ofstream num_in;
    int  fileCounter = start;
    char poseFileName[255];
    char NumFileName[255];
    char uaposeFileName[255];
    //  char frameFileName[255];
    char PdFileName[255];
    double latitude=39.1278638  ,
            longitude=117.2613378;
    snprintf(NumFileName,255,"%sAnum.num",dir);
    snprintf(uaposeFileName,255,"%sAorigin.lat",dir);
    num_in.open(NumFileName);
    num_in<<"This txt was written to Record the num between Inaccurate pose with accurate pose, "<<endl;
    num_in<<"Record the Num of accurate pose's file"<<endl<<endl66666
         <<"bin  latitude    longitude    pose in 1 m"<<endl;
    long double Cal_eps = 0.0000001;

    //读取GPS不准确的位置的原始经纬度,并寻找在生成的间隔1m的数据中相对应的数据
    uapose_out.open(uaposeFileName);
    std::string LenStr;
    for(;;){
        if((!uapose_out.good())||(!getline(uapose_out, LenStr))) break;
        unsigned int Found = 0;
        int Pos = 0;
        std::vector<double> TempVec;
        for (unsigned int i=0; i<LenStr.length()/2; ++i)
        {
            Found = LenStr.find(' ',Pos);
            std::string TempData= LenStr.substr(Pos, Found-Pos);
            TempVec.push_back(atof(TempData.c_str()));
            Pos = Found +1;
        }
        latitude = TempVec[2];
        longitude = TempVec[3];
        cout<<"latitude"<<setiosflags(std::ios::left)<<std::setprecision(10)<<latitude<<endl;
        cout<<"longitude"<<setiosflags(std::ios::left)<<std::setprecision(10)<<longitude<<endl;
        fileCounter = start;
        for (;;) {
            if (end > -1 && fileCounter > end) break; // 'nuf read
#ifndef _MSC_VER
            //name
            //     snprintf(frameFileName,255,"%sscan%.5d.frames",dir,fileCounter);
            snprintf(PdFileName,255,"%sfile3d/scan%.5d.3d",dir,fileCounter);
            snprintf(poseFileName,255,"%sfile3d/scan%.5d.pose",dir,fileCounter++);
# else
            //   snprintf(frameFileName,255,"%sscan%.5d.frames",dir,fileCounter);
            snprintf(PdFileName,255,"%sscan%.5d.3d",dir,fileCounter);
            snprintf(poseFileName,255,"%sscan%.5d.pose",dir,fileCounter);
#endif
            //Read the lat and lon from the scan*****.pose
            pose_out.open(poseFileName);
            while(!pose_out.good()) break;
            long double  allPose[11];
            double PoseOne;
            int i=0;
            while(pose_out.good()){
                pose_out>>allPose[i++];
            }
            pose_out.close();
            pose_out.clear();
            //cout<<"double_abs(allPose[9]-latitude)"<<double_abs(allPose[9]-latitude)<<endl;
            if(double_abs(allPose[9]-latitude)<Cal_eps
                    &&double_abs(allPose[10]-longitude)<Cal_eps){
                cout<<"find it"<<endl;
                num_in<<TempVec[0]<<"   "<<setiosflags(std::ios::left)<<std::setprecision(10)<<latitude<<"  "<<longitude<<"        "<<fileCounter-1<<endl;
                break;
            }
        }
    }
    num_in.close();
    num_in.clear();

    cout << " done." << endl;
}
