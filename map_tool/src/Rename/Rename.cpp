//#include <QCoreApplication>
#include <stdio.h>
#include <fstream>
#include <iostream>
using std::cout;
using std::cerr;
using std::endl;
using std::ifstream;
using std::ofstream;

#include <string.h>
#include <stdlib.h>
#ifndef _MSC_VER
#include <getopt.h>
#include <unistd.h>
#else
#include "XGetopt.h"
#endif
/**
 * function: 用来将文件进行重命名，将编号为1-300的点云数据从301-600再命名一次。
 *
 * */

int parseArgs(int argc,char **argv, char dir[255], int& start, int& end){
    start   = 0;
    end     = -1; // -1 indicates no limitation

    int  c;
    // from unistd.h
    extern char *optarg;
    extern int optind;

    cout << endl;
    while ((c = getopt (argc, argv, "s:e:")) != -1)
        switch (c)
        {
        case 's':
            start = atoi(optarg);
            if (start < 0) { cerr << "Error: Cannot start at a negative scan number.\n"; exit(1); }
            break;
        case 'e':
            end = atoi(optarg);
            if (end < 0)     { cerr << "Error: Cannot end at a negative scan number.\n"; exit(1); }
            if (end < start) { cerr << "Error: <end> cannot be smaller than <start>.\n"; exit(1); }
            break;
        }

    if (optind != argc-1) {
        cerr << "\n*** Directory missing ***\n" << endl;
        cout << endl
             << "Usage: " << argv[0] << "  [-s NR] [-e NR] directory" << endl << endl;

        cout << "  -s NR   start at scan NR (i.e., neglects the first NR scans)" << endl
             << "          [ATTENTION: counting starts with 0]" << endl
             << "  -e NR   end after scan NR" << "" << endl
             << endl;
        cout << "Reads pose files from directory/scan???.pose and converts them to directory/scan???.frames to be used by show." << endl;
        abort();
    }
    strncpy(dir,argv[optind],255);

#ifndef _MSC_VER
    if (dir[strlen(dir)-1] != '/') strcat(dir,"/");
#else
    if (dir[strlen(dir)-1] != '\\') strcat(dir,"\\");
#endif
    return 0;
}


int main(int argc, char *argv[])
{
    argc = 6;
    argv[0] = "./Rename3d";
    argv[1] = "-s";
    argv[2] = "0";
    argv[3] = "-e";
    argv[4] = "46";
    argv[5] = "/home/qy/Clont_git/pcl/build/file3d-10030-10190name";
    int start = 0, end = -1;
    char dir[255];
    parseArgs(argc, argv, dir, start, end);
    int first_new_number = start + 241 + 50000;
    int new_number =first_new_number ;
    int  fileCounter = start;
    char poseFileName[255];
    char frameFileName[255];
    char PdFileName[255];
    char new_poseFileName[255];
    char new_frameFileName[255];
    char new_PdFileName[255];
    for (;;) {
        if (end > -1 && fileCounter > end) break; // 'nuf read

#ifndef _MSC_VER
        //old_name
        snprintf(frameFileName,255,"%sscan%.5d.frames",dir,fileCounter);
        snprintf(PdFileName,255,"%sscan%.5d.3d",dir,fileCounter);
        snprintf(poseFileName,255,"%sscan%.5d.pose",dir,fileCounter);


        snprintf(new_frameFileName,255,"%sscan%.5d.frames",dir, new_number);
        snprintf(new_PdFileName,255,"%sscan%.5d.3d",dir, new_number);
        snprintf(new_poseFileName,255,"%sscan%.5d.pose",dir, new_number);


# else
        //old_name
        snprintf(frameFileName,255,"%sscan%.5d.frames",dir,fileCounter);
        snprintf(PdFileName,255,"%sscan%.5d.3d",dir,fileCounter);
        snprintf(poseFileName,255,"%sscan%.5d.pose",dir,fileCounter);
        //new_name
        //    int new_number = 2*(end - start) - fileCounter + 1 ;
        int new_number =2*(fileCounter - start) + 1 ;
        snprintf(new_frameFileName,255,"%sscan%.5d.frames",dir, new_number);
        snprintf(new_PdFileName,255,"%sscan%.5d.3d",dir, new_number);
        snprintf(new_poseFileName,255,"%sscan%.5d.pose",dir, new_number);

#endif
        fileCounter++;
        new_number++;
        if (rename(PdFileName, new_PdFileName) == 0 && rename(frameFileName, new_frameFileName) == 0
                && rename(poseFileName, new_poseFileName) == 0)
            printf("已经把文件 %s 修改为 %s.\n", PdFileName, new_PdFileName);

    }
    fileCounter = first_new_number;
 cout<<"fileCounter: "<<fileCounter<<endl;
    for (;;) {
        if (end > -1 && fileCounter > end+first_new_number) break; // 'nuf read

#ifndef _MSC_VER
        //old_name
        snprintf(frameFileName,255,"%sscan%.5d.frames",dir,fileCounter);
        snprintf(PdFileName,255,"%sscan%.5d.3d",dir,fileCounter);
        snprintf(poseFileName,255,"%sscan%.5d.pose",dir,fileCounter);
        //new_name
        //    int new_number = 2*(end - start) - fileCounter + 1 ;
        // int new_number =2*(fileCounter - start)  +100 ;

        int new_number = (fileCounter  -50000);
        snprintf(new_frameFileName,255,"%sscan%.5d.frames",dir, new_number);
        snprintf(new_PdFileName,255,"%sscan%.5d.3d",dir, new_number);
        snprintf(new_poseFileName,255,"%sscan%.5d.pose",dir, new_number);


# else
        //old_name
        snprintf(frameFileName,255,"%sscan%.5d.frames",dir,fileCounter);
        snprintf(PdFileName,255,"%sscan%.5d.3d",dir,fileCounter);
        snprintf(poseFileName,255,"%sscan%.5d.pose",dir,fileCounter);
        //new_name
        //    int new_number = 2*(end - start) - fileCounter + 1 ;
        int new_number =2*(fileCounter - start) + 1 ;
        snprintf(new_frameFileName,255,"%sscan%.5d.frames",dir, new_number);
        snprintf(new_PdFileName,255,"%sscan%.5d.3d",dir, new_number);
        snprintf(new_poseFileName,255,"%sscan%.5d.pose",dir, new_number);

#endif
        fileCounter++;

        if (rename(PdFileName, new_PdFileName) == 0 && rename(frameFileName, new_frameFileName) == 0
                && rename(poseFileName, new_poseFileName) == 0)
            printf("已经把文件 %s 修改为 %s.\n", PdFileName, new_PdFileName);

    }
    cout << " done." << endl;
}
