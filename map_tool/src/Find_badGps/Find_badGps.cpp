//#include <QCoreApplication>
#include <stdio.h>
#include <fstream>
#include <iostream>
#include <iostream>
#include <string>
#include<fstream>
#include<algorithm>  //泛型指针头文件
#include<list>
#include<deque>
#include<map>
#include<vector>
#include<set>
#include<functional>//使用事先定义的function object
#include<iterator>
#include <fstream>
#include <cstdlib>
#include <stdlib.h>
#include<math.h>
#include <cstdlib>
#include <ctime>
#include <iomanip>
using std::cout;
using std::cerr;
using std::endl;
using std::ifstream;
using std::ofstream;

#include <string.h>
#include <stdlib.h>
#ifndef _MSC_VER
#include <getopt.h>
#include <unistd.h>
#else
#include "XGetopt.h"
#endif
/**
 * function: 寻找不合适的bin序号对应的pose文件以及序号。
 *main :
 * */

int parseArgs(int argc,char **argv, char dir[255], int& rtk_flag){

    rtk_flag=1;
    int  c;
    // from unistd.h
    extern char *optarg;
    extern int optind;

    cout << endl;
    while ((c = getopt (argc, argv, "s:e:")) != -1)
        switch (c)
        {
        case 's':
            rtk_flag = atoi(optarg);
        }

    if (optind != argc-1) {
        cerr << "\n*** Directory missing ***\n" << endl;
        cout << endl
             << "Usage: " << argv[0] << " [-s flag] directory" << endl << endl;
        abort();
    }
    strncpy(dir,argv[optind],255);

#ifndef _MSC_VER
    if (dir[strlen(dir)-1] != '/') strcat(dir,"/");
#else
    if (dir[strlen(dir)-1] != '\\') strcat(dir,"\\");
#endif
    return 0;
}

double double_abs(double x){
    return x<0?(-x):x;
}

int main(int argc, char *argv[])
{
    argc = 4;
    argv[0] = "./Find_badGps";
    argv[1] = "-s";
    argv[2] = "110";
    argv[3] = "/home/qy/Clont_git/pcl/build/";
    int rtk_flag = 0;
    char dir[255];
    parseArgs(argc, argv, dir, rtk_flag);

    ifstream  bin_out;
    ofstream num_in;
    double last_rtkflag = rtk_flag;
    double atouanum = 0;
    int bad_num = 0;
    char NumFileName[255];
    char binFileName[255];
    //  char frameFileName[255];
    char PdFileName[255];

    snprintf(NumFileName,255,"%sAbadbin_number.num",dir);
    snprintf(binFileName,255,"%sbin/pose.txt",dir);
    num_in.open(NumFileName);
    num_in<<"This txt was written to Record the num between Inaccurate pose, "<<endl;
    num_in<<"Record the Num of accurate pose's file"<<endl<<endl;

    //读取GPS不准确的位置的原始经纬度,并寻找在生成的间隔1m的数据中相对应的数据
    bin_out.open(binFileName);
    std::vector<std::vector<double> > ALLbin;
    int iter = 0;
    int end_count = 0;
    bool end_flag = 0;
    std::string LenStr;
    for(;;){
        if((!bin_out.good())||(!getline(bin_out, LenStr))) break;
        unsigned int Found = 0;
        int Pos = 0;
        std::vector<double> TempVec;
        for (unsigned int i=0; i<LenStr.length()/2; ++i)
        {
            Found = LenStr.find(' ',Pos);
            std::string TempData= LenStr.substr(Pos, Found-Pos);
            TempVec.push_back(atof(TempData.c_str()));
            Pos = Found +1;
        }
        ALLbin.push_back(TempVec);
        if(TempVec[0]>31)
        {
            if((last_rtkflag == rtk_flag) && ( TempVec[1]!=rtk_flag)){
                num_in<<bad_num<<" Start unaccurate:  ";
                for(int ii = 0; ii<ALLbin.size();ii++){
                    if(TempVec[0]-40==ALLbin[ii][0]){
                        for(unsigned int i = 0; i<11; i++)
                            num_in<<setiosflags(std::ios::left)<<std::setprecision(10)<<ALLbin[ii][i]<<" ";
                      num_in<<'\n';
                    break;}

                }
            }
            if((last_rtkflag!=rtk_flag) &&(TempVec[1]==rtk_flag)){
                atouanum = TempVec[0] + 100 ;
                end_flag = 1;
            }

            if(atouanum== TempVec[0] && end_flag == 1){
                end_flag = 0;
                num_in<<bad_num++<<" end   unaccurate:  ";
                for(unsigned int i = 0; i<11; i++)
                    num_in<<setiosflags(std::ios::left)<<std::setprecision(10)<<TempVec[i]<<" ";
                num_in<<'\n'<<endl;
            }
        }
//        else{
//            if((last_rtkflag == rtk_flag) && ( TempVec[1]!=rtk_flag)){
//                num_in<<bad_num<<" Start unaccurate: ";
//                for(unsigned int i = 0; i<TempVec.size(); i++)
//                    num_in<<ALLbin[TempVec[0]][i]<<" ";
//                cout<<endl;
//            }
//            if((last_rtkflag!=rtk_flag) &&(TempVec[1]==rtk_flag)){
//                atouanum = TempVec[0] + 100;
//            }
//            if(TempVec[0]==atouanum){
//                num_in<<bad_num++<<" end   unaccurate: ";
//                for(unsigned int i = 0; i<TempVec.size(); i++)
//                    num_in<<ALLbin[TempVec[0]][i]<<" ";
//                cout<<endl<<endl;
//            }
//        }
        last_rtkflag = TempVec[1];
        std::cout<<"Iter "<<iter++
                <<endl;
        if(iter==1359)
        {
            cout<<"hello"<<endl;
        }
    }
    num_in.close();
    num_in.clear();

    cout << " done." << endl;
}
