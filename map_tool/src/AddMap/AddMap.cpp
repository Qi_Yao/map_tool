//#include <QCoreApplication>
#include <stdio.h>
#include <fstream>
#include <iostream>
#include <dirent.h>
#include <vector>
using std::cout;
using std::cerr;
using std::endl;
using std::ifstream;
using std::ofstream;

#include <string.h>
#include <stdlib.h>
#ifndef _MSC_VER
#include <getopt.h>
#include <unistd.h>
#else
#include "XGetopt.h"
#endif
/**
 * function:将两个Mapbox文件夹内的内容复制在一起，相同名字的直接将内容叠加，不同名字的直接复制过来
 *
 * */

int parseArgs(int argc,char **argv, char dir[255], int& start, int& end){
    start   = 0;
    end     = -1; // -1 indicates no limitation

    int  c;
    // from unistd.h
    extern char *optarg;
    extern int optind;

    cout << endl;
    while ((c = getopt (argc, argv, "s:e:")) != -1)
        switch (c)
        {
        case 's':
            start = atoi(optarg);
            if (start < 0) { cerr << "Error: Cannot start at a negative scan number.\n"; exit(1); }
            break;
        case 'e':
            end = atoi(optarg);
            if (end < 0)     { cerr << "Error: Cannot end at a negative scan number.\n"; exit(1); }
            if (end < start) { cerr << "Error: <end> cannot be smaller than <start>.\n"; exit(1); }
            break;
        }

    if (optind != argc-1) {
        cerr << "\n*** Directory missing ***\n" << endl;
        cout << endl
             << "Usage: " << argv[0] << "  [-s NR] [-e NR] directory" << endl << endl;

        cout << "  -s NR   start at scan NR (i.e., neglects the first NR scans)" << endl
             << "          [ATTENTION: counting starts with 0]" << endl
             << "  -e NR   end after scan NR" << "" << endl
             << endl;
        cout << "Reads pose files from directory/scan???.pose and converts them to directory/scan???.frames to be used by show." << endl;
        abort();
    }
    strncpy(dir,argv[optind],255);

#ifndef _MSC_VER
    if (dir[strlen(dir)-1] != '/') strcat(dir,"/");
#else
    if (dir[strlen(dir)-1] != '\\') strcat(dir,"\\");
#endif
    return 0;
}


int readFileList(const char *basePath, std::vector<std::string>& files )
{
    DIR *dir;
    struct dirent *ptr;
    char base[1000];

    if ((dir=opendir(basePath)) == NULL)
    {
        perror("Open dir error...");
        exit(1);
    }

    while ((ptr=readdir(dir)) != NULL)
    {
        if(strcmp(ptr->d_name,".")==0 || strcmp(ptr->d_name,"..")==0)    ///current dir OR parrent dir
            continue;
        else if(ptr->d_type == 8)    ///file
        {
            char filename[200];
            if(ptr->d_name[0] != '.')
            {
                //                sprintf(filename, "%s%s",basePath,ptr->d_name);
                files.push_back(ptr->d_name);
            }
        }
        //        else if(ptr->d_type == 10)    ///link file
        //            printf("d_name:%s/%s\n",basePath,ptr->d_name);
        //        else if(ptr->d_type == 4)    ///dir
        //        {
        //            memset(base,'\0',sizeof(base));
        //            strcpy(base,basePath);
        //            strcat(base,"/");
        //            strcat(base,ptr->d_name);
        //            readFileList(base);
        //        }
    }
    closedir(dir);
    return 1;
}

/**
 * function:将两个Mapbox文件夹内的内容复制在一起，相同名字的直接将内容叠加，不同名字的直接复制过来
 *
 * */
int main(int argc, char *argv[])
{
    char AddDir[255];
    char AddedDir[255];
     char dir[255];
     strncpy(dir, "/home/qy/Clont_git/pcl/build",255);
     ofstream  infile;
     ifstream readfile,findfile;
     int num = 1;

    std::string FileBoxRawAdd =  "/home/qy/Clont_git/Data/try/";  //添加的文件夹地址
    std::string FileBoxRawAdded =  "/home/qy/Clont_git/Data/try2/";// 目标文件夹地址
    ///!!!注意在文件路径后加上“/”
    std::vector<std::string> files;
    files.resize(0);
    readFileList(FileBoxRawAdd.c_str(), files );

    int size = files.size();

    for (int i = 0;i < size;i++)
    {
         readfile.open( (FileBoxRawAdd+files[i]).c_str());
        if(!readfile){
            std::cerr<<"Can't find "<<files[i]<<endl<<endl;
        }
        else{
            cout<<num++<<endl;
            findfile.open( (FileBoxRawAdded +files[i]).c_str());
            if(findfile.is_open()){
                findfile.close();
                findfile.clear();
                ofstream outfile((FileBoxRawAdded +files[i]).c_str(), std::ios_base::app);
                while(readfile.good()){
                    double map[3];
                    readfile>>map[0]>>map[1]>>map[2];
                    if(!readfile.good()) break;
                        if(outfile.is_open()){
                            outfile<<map[0]<<" "<<map[1]<<" "<<map[2]<<endl;
                        }
                }
                outfile.close();
                outfile.clear();
            }
                else
                {
                    findfile.close();
                    findfile.clear();
                    ofstream outfile( (FileBoxRawAdded +files[i]).c_str());
                    while(readfile.good()){
                        double map[3];
                        readfile>>map[0]>>map[1]>>map[2];
                        if(!readfile.good()) break;
                            if(outfile.is_open()){
                                outfile<<map[0]<<" "<<map[1]<<" "<<map[2]<<endl;
                            }
                    }
                outfile.close();
                outfile.clear();
            }
        }
        readfile.close();
        readfile.clear();
    }

/**
  *这是之前使用的方法，假设文件夹中最大的序号一定在-1000-1000之间，通过遍历的方法复制所有文件。
  * 但是这个方法的弊端是不能够确定是否能够复制所有的文件
  *
  *
 **/
if(0)
{
    for(int BoxCol = -1000; BoxCol<1000;BoxCol++)
    {
        for(int BoxRow= -1000; BoxRow<1000; BoxRow++)
        {
            sprintf(AddedDir, "%s/MapBoxRaw/%d_%d",dir, BoxCol, BoxRow);//convert int to char//
            readfile.open(AddedDir);
            if(readfile.is_open())
            {
                cout<<num++<<endl;
                sprintf(AddDir, "%s/MapBoxRaw14/%d_%d",dir, BoxCol, BoxRow);//convert int to char//
                findfile.open(AddDir);
                if(findfile.is_open()){
                    findfile.close();
                    findfile.clear();
                    ofstream outfile(AddDir, std::ios_base::app);
                    while(readfile.good()){
                        double map[3];
                        readfile>>map[0]>>map[1]>>map[2];
                        if(!readfile.good()) break;
                            if(outfile.is_open()){
                                outfile<<map[0]<<" "<<map[1]<<" "<<map[2]<<endl;
                            }
                    }
                    outfile.close();
                    outfile.clear();
                }
                    else
                    {
                        findfile.close();
                        findfile.clear();
                        ofstream outfile(AddDir);
                        while(readfile.good()){
                            double map[3];
                            readfile>>map[0]>>map[1]>>map[2];
                            if(!readfile.good()) break;
                                if(outfile.is_open()){
                                    outfile<<map[0]<<" "<<map[1]<<" "<<map[2]<<endl;
                                }
                        }
                    outfile.close();
                    outfile.clear();
                }
                readfile.close();
                readfile.clear();
            }
        }
    }
}
    cout << " done." << endl;
}
